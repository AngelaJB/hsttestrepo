#!/usr/bin/env python
from astropy.io import fits

class Spectrum(object):
    def __init__(self, filepath=None):
        if filepath is None:
            print("A spectrum file must "
            "be specified to create a spectrum.")
        self.filepath = filepath
        hdulist = fits.open(filepath)
        # print(hdulist.info())
        tbdata = hdulist[1].data
        self.wave = tbdata['wave']
        self.flux = tbdata['flux']       
        return None
