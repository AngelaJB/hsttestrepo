#!/usr/bin/env python

from firstmodule_spectrum.spectrum import Spectrum
input_object ="G141_23158"
input_filename ="cosmos-01-%s.1D.fits" % input_object
output_filename="lambda_vs_flux_%s.pdf"% input_object
infolderpath="../data/"
outfolderpath="../Docs/"
folder_input_filename = "%s%s" % (infolderpath,input_filename)
folder_output_filename = "%s%s" % (outfolderpath,output_filename)
print("input file =%s",folder_input_filename)
print("output file =%s",folder_output_filename)

s = Spectrum(folder_input_filename)
import matplotlib.pyplot as plt

plt.plot(s.wave,s.flux)
plt.xlabel('wavelength [nm]')
plt.ylabel('flux [units]')
graphtitle = "spectra- object %s" % input_object
plt.title(graphtitle)
plt.savefig(folder_output_filename)
plt.close() 


